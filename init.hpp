#ifndef INIT_HPP_INCLUDED
#define INIT_HPP_INCLUDED
#include "ublas.hpp"
void init(ublas::vector<double>& debye_vec,ublas::vector<double>& parameter_vec);


#endif // INIT_HPP_INCLUDED
