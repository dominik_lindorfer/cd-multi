#ifndef CORRELATION_HPP_INCLUDED
#define CORRELATION_HPP_INCLUDED
#include "ublas.hpp"
void correlation(ublas::vector<double> parameter_vec, ublas::vector<double>& CIm);


#endif // CORRELATION_HPP_INCLUDED
