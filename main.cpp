#include <iostream>
#include <iomanip>
#include <cmath>
#include <string>
#include <fstream>
#include "createNBNDfile.hpp"
#include "init.hpp"
#include "dipole.hpp"
#include "excmatrix.hpp"
#include "absorption.hpp"
#include "absorptionCD.hpp"
#include "lineshape.hpp"
#include "randomgauss.hpp"
#include "correlation.hpp"
#include "ublas.hpp"
#include "lineshape_dl.hpp"

using namespace std;

int main()
{

    ublas::vector<double> debye_vec;    //exp. dipolestrength for CHL & CLA ... initialized in init.dat
                                        //...could be expanded to a debyevector?!!
    ublas::vector<double> sitevec;
    ublas::vector<double> parameter_vec;    //holds parameters defined in init.dat
    ublas::vector<string> chlname_vec;      //get all the names of CHL & CLA & Carotenoids

    ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat;    //sets up the matrix (vector of matrices actually)
                                                                          //for the transition dipole moments
    ublas::matrix<double> evecs;            //matrix&vector for Eigenvectors & Eigenvalues
    ublas::vector<double> evals;

    init(debye_vec,parameter_vec);          //has to be before getdipoles!!
    getdipoles( dipolemat, debye_vec , chlname_vec);
    getSiteEnergies(sitevec, chlname_vec);
    diagonalizeexcmatrix(evecs, evals, chlname_vec, parameter_vec, sitevec);

    cout<<"Evals: "<<evals<<" Evecs: "<<evecs<<endl;
    cout<<"SiteEnergies from SiteVec: "<<sitevec<<endl;
    //cout<<"Evecs main: "<<evecs<<endl;        //errorchecking
    //cout<<"Evals main: "<<evals<<endl;
    //cout<<"Dipolemat main: "<<dipolemat<<endl;
    cout<<"Debye Vector: "<<debye_vec<<endl;
    ublas::vector<double> alpha;
    ublas::vector<double> CD;
    //absorption(evecs, evals, dipolemat, parameter_vec, alpha);
    //cout<<dipolemat(0)<<endl;

    //Ausgliederung von CIm & G(t)
    ublas::vector<double> CIm;
    correlation(parameter_vec, CIm);

    ublas::vector<double> GReals;
    ublas::vector<double> GImags;
    //CHANGE G(t) here
    //FT_Gt_WZ(GReals,GImags,8192.0 / 16.0 ,8192.0*512,parameter_vec);  //HEOM
    FT_Gt_heom_WZ(GReals,GImags,8192.0*512);

    /*
    ofstream Cre;
    Cre.open("Cre.txt");
    for(double i = 0; i <= 100000; i = i+0.1){
            Cre<<i<<" "<<J_WZ(i)<<endl;
    }
    Cre.close();*/


    CDabsorption(evecs, evals, dipolemat, parameter_vec, CD, alpha, CIm, GReals, GImags);
    //StaticDisorder(sitevec,evals,evecs,parameter_vec,chlname_vec,dipolemat);

    /*
    ofstream testre;
    testre.open("testre.dat");
    for(int i = 0; i<500; i = i+5){
        testre<<i<<"  "<<C_Re_WZ(i,77)<<endl;
    }
    testre.close();

    ublas::vector<double> GRealsHEOM, GImagsHEOM, DMRealsHEOM;

    double ntime = 8192.0*512;

    FT_Gt_heom_WZ(GRealsHEOM,GImagsHEOM,ntime);

    cout<<"ntime: "<<ntime<<endl;
    cout<<"GR: "<<GRealsHEOM.size()<<endl;
    cout<<"GI: "<<GImagsHEOM.size()<<endl;

    //DM_WZ_HEOM(0,DMRealsHEOM,GRealsHEOM,GImagsHEOM,8192.0 / 16.0 ,8192.0*512,CIm,evecs,evals,parameter_vec,dipolemat);


    for(int i = 0; i < 10; i++){
        cout<<"i = "<<i<<" "<<GR(i)<<" "<<GI(i)<<endl;
    }

    for(int i = ntime/2.0-1; i > ntime/2.0-10; i--){
        cout<<"i = "<<i<<" "<<GR(i)<<" "<<GI(i)<<endl;
    }*/

    //Ft_Gt_heom_LS(GR,GI);

    return 0;
}
