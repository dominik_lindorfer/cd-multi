#ifndef ABSORPTION_HPP_INCLUDED
#define ABSORPTION_HPP_INCLUDED
#include "ublas.hpp"
void absorption(ublas::matrix<double>& evecs, ublas::vector<double>& evals, ublas::vector< ublas::vector< ublas::vector<double> > >& dipolemat,ublas::vector<double> parameter_vec, ublas::vector<double>& alpha);

#endif // ABSORPTION_HPP_INCLUDED
