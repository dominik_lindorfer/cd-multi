#include "ublas.hpp"
#include <iostream>
#include <fstream>
#include "lineshape.hpp"
#include "correlation.hpp"
using namespace std;

void absorption(ublas::matrix<double>& evecs, ublas::vector<double>& evals, ublas::vector< ublas::vector< ublas::vector<double> > >& dipolemat, ublas::vector<double> parameter_vec, ublas::vector<double>& alpha){


    //cout<<eval.size()<<endl;
    ublas::vector< ublas::vector<double> > trans_dipole_moment(evals.size());

    ublas::zero_vector<double> zero(3);

    for(int i=0;i<trans_dipole_moment.size();i++){
        trans_dipole_moment(i)=zero;    //zerovectors so += can be used!
    }

    //cout<<trans_dipole_moment<<endl;
    //cout<<dipolemat(0)(0)<<endl;

    for(int i = 0; i<trans_dipole_moment.size(); i++){              //check if error occurs!!
        for(int j = 0; j<trans_dipole_moment.size(); j++){
            trans_dipole_moment(i)+=dipolemat(0)(j)*evecs(i,j);      //careful! dipolemat(0) because it contains the ei_dipole
        }                                                           //there are only 2 entries until now!!!
    }
    //cout<<"Dipolemat"<<dipolemat(0)(0)<<endl;     //errorchecking
    //cout<<"Evec"<<evec(0,0)<<endl;                //errorchecking

    //Here is the outputpart, this needs to be changed when LS-theory is imported
    ofstream OD;
    OD.open("OD.dat",ios::out);
    //cout<<trans_dipole_moment<<endl;  //errorchecking

    ublas::vector<double> DMReals;
    //ublas::vector<double> alpha;
    ublas::vector<double> CIm;
    correlation(parameter_vec, CIm);

    ublas::vector<double> GReals;
    ublas::vector<double> GImags;
    FT_Gt_WZ(GReals,GImags,8192.0 / 16.0 ,8192.0*64,parameter_vec);

    for(int i=0;i<trans_dipole_moment.size();i++){
            DM_WZ(i,DMReals,GReals,GImags,8192.0 / 16.0 ,8192.0*64,CIm,evecs,evals,parameter_vec,dipolemat);
            alpha.resize(DMReals.size());
            alpha = alpha + DMReals * inner_prod(trans_dipole_moment(i),trans_dipole_moment(i));
    }

    //int upperlim = ntime/2.0  + te/twopi * (1.0/(600*1e-7) - wM0);      //approximatively this
    //int lowerlim = ntime/2.0  + te/twopi * (1.0/(730*1e-7) - wM0);      //Check here for errors, (int) lower/upperlim might not be equal for all States M!

    for(int i=0; i <alpha.size(); i++){
        OD<<(i-8192.0*64/2.0) * (2*3.1415926)/(8192.0 / 16.0) + 1.0/(600*1e-7) <<" "<<alpha(i)<<endl;       //values i need to be changed, it should go from [16667.7 - 13698] derweil unwichtig
    }
    OD.close();
    //myfile<<(i-ntime/2.0)*twopi/te<<" "<<DMReals(i)<<endl;

    //This was for Stickspectra
    /*for(int i=0;i<trans_dipole_moment.size();i++){
        OD<<"{ "<<eval(i)<<","<<inner_prod(trans_dipole_moment(i),trans_dipole_moment(i))<<" }"<<endl;
    }*/

}
