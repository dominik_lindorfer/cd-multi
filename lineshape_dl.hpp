#ifndef LINESHAPE_DL_HPP_INCLUDED
#define LINESHAPE_DL_HPP_INCLUDED

void Ft_Gt_heom_LS(ublas::vector<double>& GReals, ublas::vector<double>& GImags);

#endif // LINESHAPE_DL_HPP_INCLUDED
