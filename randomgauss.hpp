#ifndef RANDOMGAUSS_HPP_INCLUDED
#define RANDOMGAUSS_HPP_INCLUDED
#include "ublas.hpp"
double NormalRandom(double mean,double sigma);
double RandomizeSiteEnergies(ublas::vector<double>& SiteEnergy, double FWHM);
double StaticDisorder(ublas::vector<double> sitevec, ublas::vector<double>& evals, ublas::matrix<double>& evecs, ublas::vector<double> parameter_vec, ublas::vector<string> chlname_vec,  ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);

#endif // RANDOMGAUSS_HPP_INCLUDED
