#ifndef FOURIERINTEGRALS_HPP_INCLUDED
#define FOURIERINTEGRALS_HPP_INCLUDED
#include <string>
using namespace std;

double FourierLaplace_Real_P1(double xpara, double (*f)(double), double t);
double FourierLaplace_Real_P2(double xpara, double (*f)(double), double t);
double FourierLaplace_Complex_P1(double xpara, double (*f)(double), double t);
double FourierLaplace_Complex_P2(double xpara, double (*f)(double), double t);
double FourierLaplace( double (*function)(double) , string Complex_1_OrReal_0_Part, double t);


#endif // FOURIERINTEGRALS_HPP_INCLUDED
