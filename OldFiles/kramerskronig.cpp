#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include "ublas.hpp"
using namespace std;

//KramersKronig of CRe(w) through splitted Gauss-Chebyshev Integration

double C_RealPart (double x){   // !!!TESTFUNCTION!!!

    return (exp(-pow(x,2)));    //for exp(-x2)/(3.0 - x) testfunction && delta = 10^-6
}

//Kramers-Kronig Integration of (4.16) of Renger/Marcus...Integral was splitted into 4 Parts because Variable-Transformation was a nasty bitch

double KramersKronig_P1(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){
    //Part1 of the Kramers-Kronig Routine, performs the Integration in the Interval [wk+delta,Infinity]

    double w_trans;                     //Parameter for Variabletransformations
    w_trans = 1.0/2.0 * w + 1.0/2.0 ;    //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    w_trans = (wk+delta) / w_trans;      //Use a  t=1/x like Substitution to transform improper integral

    return(1.0/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans) * pow(w_trans,2)/(wk+delta) );
}

double KramersKronig_P2(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){
    //Part2 of the Kramers-Kronig Routine, performs the Integration in the Interval [-Infinity,-1]
    double w_trans;                      //Parameter for Variabletransformations
    w_trans = 1.0/2.0 * w + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    w_trans = -1.0 / w_trans;             //Use Substitution t=1/x to transform improper integral

    return(1.0/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans) * pow(w_trans,2) );
}

double KramersKronig_P3(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){

    double w_trans;                                                    //Parameter for Variabletransformations
    w_trans = ((wk - delta)+1.0)/2.0 * w + ((wk - delta)-1.0)/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [-1,wk+delta]
    //w_trans = -1.0 / w_trans;

    return(((wk - delta)+1.0)/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans));
}
//KramersKronig_P4() //this calculates the Principal Value Integral through a Series

//This is for the case Wmk < 0 ... VariableTransformation made problems here; Maybe not even necessary
double KramersKronig_Neg_P1(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){
    //Part1 of the Kramers-Kronig Routine for wk<0, performs the Integration in the Interval [wk+delta,1]
    double w_trans;                      //Parameter for Variabletransformations
    w_trans = (1.0 - (wk + delta))/2.0 * w + ((wk + delta) + 1.0)/2.0;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]

    return((1.0 - (wk + delta))/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans) );
}

double KramersKronig_Neg_P2(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){
    //Part1 of the Kramers-Kronig Routine for wk<0, performs the Integration in the Interval [1,Infinity]
    double w_trans;                      //Parameter for Variabletransformations
    w_trans = 1.0/2.0 * w + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    w_trans = 1.0 / w_trans;
    return(1.0/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans) * pow(w_trans,2)  );
}

double KramersKronig_Neg_P3(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec){
    //Part1 of the Kramers-Kronig Routine for wk<0, performs the Integration in the Interval [-Infinity,wk-delta]
    double w_trans;                      //Parameter for Variabletransformations
    w_trans = -1.0/2.0 * w - 1.0/2.0 ;      //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    w_trans = -(wk-delta) / w_trans;
    return( -1.0/2.0 * C_Re(w_trans,parameter_vec(0)) / (wk - w_trans) * pow(w_trans,2)/(wk-delta) );
}
//KramersKronig_Neg_P4() //this calculates the Principal Value Integral through a Series

double KramersKronig(double (*CRe)(double,double), double Wmk,ublas::vector<double> parameter_vec){      //KramersKronig of CRe(w) through splitted Gauss-Chebyshev Integration
    //Modify these Parameters for Accuracy
    double pi = 3.1415926535897932384626434;
    double n = 100000;  //#of nodes
    double error = 1e-6;
    double sum;

    vector<double> nodes(n);
    vector<double> weights(n);

    for(int i = 1; i <=n; i++){
        nodes[i-1] = cos((2.0 * i - 1)/(2.0*n) * pi);
        weights[i-1] = pi / n;
    }

    /*for(int i = 0; i < n; i++){
        sum += weights[i] * f(nodes[i]) * sqrt(1 - pow(nodes[i],2));
    }*/
    if (Wmk >= 0){
        for(int i = 0; i < n; i++){
            sum += weights[i] * KramersKronig_P1(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
            sum += weights[i] * KramersKronig_P2(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
            sum += weights[i] * KramersKronig_P3(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
        }
    }

    if (Wmk < 0){
        for(int i = 0; i < n; i++){
            sum += weights[i] * KramersKronig_Neg_P1(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
            sum += weights[i] * KramersKronig_Neg_P2(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
            sum += weights[i] * KramersKronig_Neg_P3(nodes[i], Wmk, CRe, error, parameter_vec) * sqrt(1 - pow(nodes[i],2));
        }
    }
    cout<<setprecision(25)<<"Integrated Functionvalue is:  " << sum / pi<<endl;
    return sum / pi;
}
