#ifndef FOURIERTRAFO_HPP_INCLUDED
#define FOURIERTRAFO_HPP_INCLUDED

void FourierTrafo(ublas::vector<double> Reals, ublas::vector<double> Imags, double te, double ntime);

#endif // FOURIERTRAFO_HPP_INCLUDED
