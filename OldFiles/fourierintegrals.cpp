#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

double FourierLaplace_Real_P1(double xpara, double (*f)(double), double t){

    double x;
    x = 1.0/2.0 * xpara + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]

    return ( 1.0 / 2.0 * f(x) * cos(t*x) );

}

double FourierLaplace_Real_P2(double xpara, double (*f)(double), double t){

    double x;
    x = 1.0/2.0 * xpara + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    x = 1.0 / x;                        //Transforms the Intervals [0,1] to [1,Infinity]

    return (1.0/2.0 * f(x) * cos(t*x) * pow(x,2));

}

double FourierLaplace_Complex_P1(double xpara, double (*f)(double), double t){

    double x;
    x = 1.0/2.0 * xpara + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]

    return ( 1.0 / 2.0 * f(x) * sin(t*x) );

}

double FourierLaplace_Complex_P2(double xpara, double (*f)(double), double t){

    double x;
    x = 1.0/2.0 * xpara + 1.0/2.0 ;     //Transforms the Gauss-Chebyshev Intervals [-1,1] to [0,1]
    x = 1.0 / x;                        //Transforms the Intervals [0,1] to [1,Infinity]

    return (1.0/2.0 * f(x) * sin(t*x) * pow(x,2));

}


double FourierLaplace( double (*function)(double) , string ComplexOrRealPart, double t)     //int ComplexOrRealPart
{
    //Does a Gauss-Chebyshev Integration of f(x) in [0,Infinity], Complex & Realpart are split twice respectively
    //Use int ComplexOrRealPart to distinguish between Complex (set it to 1) & Real (set it to 0) Result
    double pi = 3.1415926535897932384626434;
    double n = 100000;  //#of nodes

    vector<double> nodes(n);
    vector<double> weights(n);

    for(int i = 1; i <=n; i++){
        nodes[i-1] = cos((2.0 * i - 1)/(2.0*n) * pi);
        weights[i-1] = pi / n;
    }

    if( ComplexOrRealPart == "Real" ){
        double sum;

        for(int i = 0; i < n; i++){
            sum += weights[i] * FourierLaplace_Real_P1(nodes[i],function,t) * sqrt(1 - pow(nodes[i],2));
            sum += weights[i] * FourierLaplace_Real_P2(nodes[i],function,t) * sqrt(1 - pow(nodes[i],2));
        }
        //cout<<setprecision(25)<<"Integrated Functionvalue (Real) is:  " << sum<<endl;
        return sum;
    }
    if( ComplexOrRealPart == "Complex" ){
        double sumcomplex;

        for(int i = 0; i < n; i++){
            sumcomplex += weights[i] * FourierLaplace_Complex_P1(nodes[i],function,t) * sqrt(1 - pow(nodes[i],2));
            sumcomplex += weights[i] * FourierLaplace_Complex_P2(nodes[i],function,t) * sqrt(1 - pow(nodes[i],2));
        }
        //cout<<setprecision(25)<<"Integrated Functionvalue (Complex) is:  " << sumcomplex<<endl;
        return sumcomplex;
    }
    else {
        //cout<<"ComplexOrReal given to FourierLaplace-Function is not set right!"<<endl;
        return 0;
    }
}
