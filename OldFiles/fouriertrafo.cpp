#include "ublas.hpp"
#include <cmath>
#include <fstream>
#include <iostream>
#include <fftw3.h>

using namespace std;

void FourierTrafo(ublas::vector<double> Reals, ublas::vector<double> Imags, double te, double ntime){

    //This is the pure FFTW Part of the Testprogram -- DeVries S.305


    //these are old parameters from above, but still used here
    //double te = 8192.0 / 128.0;             //te is the time-interval used for delta; the smaller this is, the closer the interval for delta-w gets;
                                            //the interval you get is [-ntime/2 * delta , ntime/2 * delta]
                                            //if you want to increase the interval of the results, change this parameter
    //double ntime = 8192.0 * 64.0;           //this is N which defines the highest frequency we can obtain, it is w = (N-1) * delta * w; change for accurracy
    int N = ntime;    //gives the size of the Vectors you put into FFTW and get out of it; The bigger this is the more accurate the results
    double delta = te / (ntime -1);         //this sets the interval for frequency / time
    double twopi = 2.0 * 3.1415926535897932384626434;
    double spe,spe1,om,corr_real,corr_imag;
    fftw_complex *in, *out;
    fftw_plan p;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

/*    double spec[2 * (int)ntime];

  for(int i = 1; i <= ntime / 2.0 -1; i++){
     om = twopi * i / delta / ntime;
     spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
     spe1 = spe;

     spec[2*i + 1] = spe;
     spec[2*(int)ntime - 2*i+1] = spe1;
     spec[2*i+2] = 0.;
     spec[2*(int)ntime - 2*i+2] = 0.;

  }*/


    //Data alignment in in[i][j] accoriding to four1 from above
    for(int i = 1; i <= ntime/2 -1; i++){
         om = twopi * i / delta / ntime;
         spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
         spe1 = spe;

         in[i][0] = spe;
         in[(int)ntime - i][0] = spe1;

         in[i][1] = 0.;
         in[(int)ntime - i][1] = 0.;

    }
    in[0][0] = 1.0/(0.5 * twopi);

    p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);  //FFTW_MEASURE
    fftw_execute(p);

    ofstream myfile1;
    myfile1.open ("corr1.dat");

    for(int i = 0; i < ntime; i++){

        corr_real = out[i][0] / ntime * twopi / delta;
        corr_imag = out[i][1] / ntime * twopi / delta;
        myfile1 << i*delta <<" "<<corr_real<<" "<<corr_imag<<" "<<exp(-i*delta)<<endl;
    }

    myfile1.close();


    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}
