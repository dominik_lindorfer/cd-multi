#ifndef KRAMERSKRONIG_HPP_INCLUDED
#define KRAMERSKRONIG_HPP_INCLUDED
#include "ublas.hpp"
double KramersKronig(double (*CRe)(double,double), double Wmk,ublas::vector<double> parameter_vec);
double KramersKronig_P1(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double KramersKronig_P2(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double KramersKronig_P3(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double KramersKronig_Neg_P1(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double KramersKronig_Neg_P2(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double KramersKronig_Neg_P3(double w, double wk, double (*C_Re)(double,double), double delta,ublas::vector<double> parameter_vec);
double C_RealPart (double x);   // !!!TESTFUNCTION!!!

#endif // KRAMERSKRONIG_HPP_INCLUDED
