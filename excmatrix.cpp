#include <iostream>
#include "ublas.hpp"
#include <fstream>
#include <sstream>
#include "nr3.h"
#include "eigen_sym.h"

using namespace std;

void getSiteEnergies(ublas::vector<double>& sitevec, ublas::vector<string> chlname_vec){
    //at first get the site energies out of the .dat file and save it to a site-energy-vector sitevec
    ifstream Site_Energies ("Site_Energies.dat");

    double E0a,E0b,E0aBy,E0bBy,E0aQx,E0bQx,E0aBx,E0bBx;                     //may needs to be expaned if carotenoids || Qx,Bx etc. transitions are present
    string str;


    if (Site_Energies.is_open()){           //obtain E0a & E0b from Site_Energies.dat
        if(Site_Energies.good()){           //this is from the first line
            getline (Site_Energies,str);
            stringstream sstest(str);
            sstest >> E0a;
            sstest >> E0b;
            sstest >> E0aBy;
            sstest >> E0bBy;
            sstest >> E0aQx;
            sstest >> E0bQx;
            sstest >> E0aBx;
            sstest >> E0bBx;
        }
    }
    cout<<"E0a: "<<E0a<<"  E0b: "<<E0b<<" E0aBy: "<<E0aBy<<" E0bBy: "<<E0bBy<<" E0aQx: "<<E0aQx<<" E0bQx: "<<E0bQx<<" E0aBx: "<<E0aBx<<" E0bBx: "<<E0bBx<<endl;     //errorchecking
    int k=0;
    if (Site_Energies.is_open()){           //obtain siteenergies from Site_Energies.dat
        while(Site_Energies.good()){           //this is from all other lines
            getline (Site_Energies,str);
            stringstream sstest(str);
            sitevec.resize(sitevec.size()+1);
            sstest >> sitevec(k);
            k++;
        }
    }

    //resize the siteenergyvector so that there are no 0's at the end ---- this is an arbitrary and temporary choice!
    while(sitevec(sitevec.size()-1)==0){
        sitevec.resize(sitevec.size()-1);
    }
    //cout<<sitevec<<endl;        //errorchecking

    //assigns E0a & E0b to the site energies
    //cout<<chlname_vec<<endl;

    int n = sitevec.size();
    for(int i=0;i<n;i++){
        if(chlname_vec(i)=="CLA"){
            sitevec(i)=sitevec(i)+1.0/(E0a * 1e-7);
        }
        if(chlname_vec(i)=="CHL"){
            sitevec(i)=sitevec(i)+1.0/(E0b * 1e-7);
        }
        if(chlname_vec(i)=="CLABy"){
            sitevec(i)=sitevec(i)+1.0/(E0aBy * 1e-7);
        }
        if(chlname_vec(i)=="CHLBy"){
            sitevec(i)=sitevec(i)+1.0/(E0bBy * 1e-7);
        }
        if(chlname_vec(i)=="CLAQx"){
            sitevec(i)=sitevec(i)+1.0/(E0aQx * 1e-7);
        }
        if(chlname_vec(i)=="CHLQx"){
            sitevec(i)=sitevec(i)+1.0/(E0bQx * 1e-7);
        }
        if(chlname_vec(i)=="CLABx"){
            sitevec(i)=sitevec(i)+1.0/(E0aBx * 1e-7);
        }
        if(chlname_vec(i)=="CHLBx"){
            sitevec(i)=sitevec(i)+1.0/(E0bBx * 1e-7);
        }
        //Expand for Carotenoids or other tranitions!!
    }
    //cout<<"SiteVec is: "<<sitevec<<endl;  //errorchecking
}

void diagonalizeexcmatrix(ublas::matrix<double>& evec, ublas::vector<double>& eval, ublas::vector<string>& chlname_vec, ublas::vector<double>& parameter_vec, ublas::vector<double> sitevec){

    string str;
    int n = sitevec.size();                         //allocate excitonmatrix that will be diagonalized
    ublas::matrix<double> excitonmatrix(n,n);
    ublas::zero_matrix<double> tmpzeromatrix(n,n);  //to get out chunk
    //cout<<excitonmatrix1<<endl;                   //errorcheking
    excitonmatrix=tmpzeromatrix;


    for(int i=0;i<n;i++){               //set up the diagonal
        excitonmatrix(i,i)=sitevec(i);
    }

    //get the excitonic couplings out from the .dat file
    ifstream exc_coupling ("excitonic_coupling.dat");

    double u,v,w;

    if (exc_coupling.is_open()){           //obtain siteenergies from Site_Energies.dat
        while(exc_coupling.good()){           //this is from all other lines
            getline (exc_coupling,str);
            stringstream sstest(str);

            if(str == "NONE"){          //if the user writes MONOMERE into the couplings.dat-file it skips to the Monomere-Option
                break;
            }

            sstest >> u;
            sstest >> v;
            sstest >> w;
            excitonmatrix(u-1,v-1) = w;
            excitonmatrix(v-1,u-1) = w;

        }
    }
    else{   //This part was added so a Monomere can be calculated
            for(int i = 0; i<n; i++){
                for(int j = 0; j<n; j++){
                    if(i!=j){
                        excitonmatrix(i,j) = 0.0;
                    }
                }
            }

    }
    cout<<"Excitonmatrix Read In: "<<endl;
    cout<<excitonmatrix<<endl;    //errorchecking

    //-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //Here the Eigenvalueproblem is solved through the NR eigensolver "nr3.h" && eigen_sym
    //-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    //NR MatrixClass .... this is just a temporary object
    MatDoub tmp_excmat(n,n);

    //Copy the excitonmatrix to the NR-Matrixclass

    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            tmp_excmat[i][j]=excitonmatrix(i,j);
        }
    }

    //NR eigenvalueproblem solver - this creats a class
    Symmeig sol(tmp_excmat);
    MatDoub tmp_evec(n,n);
    VecDoub tmp_eval(n);
	tmp_eval = sol.d;     //obtain solutions from the solverclass
    tmp_evec = sol.z;

    eval.resize(n);         //resize original working objects
    evec.resize(n,n);

    //this is for errorchecking!! the matrix which is diagonalized is just printed out
    /*
    //cout << fixed << setprecision(3);
	cout << "The matrix read in is of size " << n << " x " << n <<" and contains the following elements" << endl << endl;
	// print out the matrix that was read into the program
	for(int i=0;i<n;i++) {
		for(int j=0;j<n;j++) {
			cout << setw(14) << excmat[i][j];
		}
		cout << endl;
	}
	cout << endl;*/


    //Print & Copy all the results
    cout << endl << "Eigenvalues: " << endl << endl;
    for(int i=0;i<n;i++) {
		cout << setw(14) << tmp_eval[i];
		cout << endl;
		eval(i)=tmp_eval[i];
	}

	cout << endl << "EigenVectors" << endl << endl;
	for(int i=0;i<n;i++) {
        cout<<"{";
        for(int j=0;j<n;j++){
            cout << setw(14) << tmp_evec[j][i]<<",";
            evec(i,j)=tmp_evec[j][i];

        }
        cout<<"}";
        cout<<endl;
        cout<<endl;
	}
}
