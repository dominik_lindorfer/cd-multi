#include "random.hpp"
#include "ublas.hpp"
#include <cmath>
#include <fstream>
#include "excmatrix.hpp"
#include "absorption.hpp"
#include "absorptionCD.hpp"
#include "lineshape.hpp"
#include "correlation.hpp"

using namespace std;

double NormalRandom(double mean,double sigma){
 typedef boost::normal_distribution<double> NormalDistribution;
 typedef boost::mt19937 RandomGenerator;
 typedef boost::variate_generator<RandomGenerator&, \
                         NormalDistribution> GaussianGenerator;

  /* Initiate Random Number generator with seed 47382917263548729976411 */
  static RandomGenerator rng(static_cast<unsigned> (47382917263548729976411));

  /* Choose Normal Distribution */
  NormalDistribution gaussian_dist(mean, sigma);

  /* Create a Gaussian Random Number generator
   *  by binding with previously defined
   *  normal distribution object
   */
  GaussianGenerator generator(rng, gaussian_dist);

  // sample from the distribution
  return generator();
}

double RandomizeSiteEnergies(ublas::vector<double>& SiteEnergy, double FWHM){
    for(int i = 0; i < SiteEnergy.size(); i++){
        SiteEnergy(i) = NormalRandom(SiteEnergy(i),FWHM);
    }
}

double StaticDisorder(ublas::vector<double> sitevec, ublas::vector<double>& evals, ublas::matrix<double>& evecs, ublas::vector<double> parameter_vec, ublas::vector<string> chlname_vec,  ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){
    //Static Disorder
    ofstream ODsum;
    ODsum.open("ODDis.dat");

    ofstream CDsum;
    CDsum.open("CDDis.dat");

    ublas::vector<double> alpha;
    ublas::vector<double> alpha_sum;
    ublas::vector<double> CD;
    ublas::vector<double> CD_sum;

    ublas::vector<double> sitevec_random;
    sitevec_random = sitevec;

    //Ausgliederung von CIm & G(t)
    ublas::vector<double> CIm;
    correlation(parameter_vec, CIm);

    ublas::vector<double> GReals;
    ublas::vector<double> GImags;
    FT_Gt_WZ(GReals,GImags,8192.0 / 16.0 ,8192.0*512,parameter_vec);



    for(int i = 0; i<parameter_vec(3); i++){        //parameter_vec(3) holds #nrand
        //cout<<"SiteEnergies-Original: "<<sitevec_modfied<<endl;
        //cout<<parameter_vec(2)<<endl;     //contains FWHM = 120cm^-1
        RandomizeSiteEnergies(sitevec_random,120);  //120 set temporarily
        diagonalizeexcmatrix(evecs, evals, chlname_vec, parameter_vec, sitevec_random);
        //cout<<"Evals-Modified: "<<sitevec_modfied<<endl;
        //absorption(evecs, evals, dipolemat, parameter_vec, alpha);
        CDabsorption(evecs, evals, dipolemat, parameter_vec, CD, alpha, CIm, GReals, GImags);

        CD_sum.resize(CD.size());
        CD_sum = CD_sum + CD;
        alpha_sum.resize(alpha.size());
        alpha_sum = alpha_sum + alpha;

        sitevec_random = sitevec;
    }

    alpha_sum = alpha_sum / parameter_vec(3);
    for(int i = 0; i< alpha_sum.size(); i++){
        ODsum<<0.01 * 1.0/(1.0/(300*1e-7) - i*(2*3.1415926)/(512.0))<<" "<<alpha_sum(i)<<endl;
        //ODsum<<(i-8192.0*64/2.0) * (2*3.1415926)/(8192.0 / 16.0) + 1.0/(600*1e-7) <<" "<<alpha_sum(i)<<endl;
    }

    CD_sum = CD_sum / parameter_vec(3);
    for(int i = 0; i< CD_sum.size(); i++){
        CDsum<<0.01 * 1.0/(1.0/(300*1e-7) - i*(2*3.1415926)/(512.0))<<" "<<CD_sum(i)<<endl;
        //CDsum<<(i-8192.0*64/2.0) * (2*3.1415926)/(8192.0 / 16.0) + 1.0/(600*1e-7) <<" "<<CD_sum(i)<<endl;
    }

    ODsum.close();
    CDsum.close();
}
