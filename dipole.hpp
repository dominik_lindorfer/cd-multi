#ifndef DIPOLE_HPP_INCLUDED
#define DIPOLE_HPP_INCLUDED
#include "ublas.hpp"
using namespace std;
void getdipoles(ublas::vector< ublas::vector< ublas::vector<double> > >& dipolemat, ublas::vector<double> debye_vec, ublas::vector<string>& chlname_vec);


#endif // DIPOLE_HPP_INCLUDED
