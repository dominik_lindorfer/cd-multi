#ifndef LINESHAPE_HPP_INCLUDED
#define LINESHAPE_HPP_INCLUDED
#include <string>
#include "ublas.hpp"
using namespace std;

double J_WZ(double w);
double n_WZ(double w, double T);
double C_Re_WZ (double w, double T);

double y_MNKL (int M, int N, int K, int L, ublas::matrix<double> evecs,  ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);
double tau_WZ (int M, ublas::vector<double> sitevec, ublas::matrix<double> evecs,  ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);
double wM0_WZ ( int M, ublas::vector<double> evals, ublas::matrix<double> evecs, ublas::vector<double> parameter_vec, ublas::vector<double> CIm, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);

void FT_Gt_WZ(ublas::vector<double>& GReals, ublas::vector<double>& GImags, double te, double ntime, ublas::vector<double> parameter_vec);
double DM_WZ(int M, ublas::vector<double>& DMReals, ublas::vector<double> GReal, ublas::vector<double> GImag, double te, double ntime,ublas::vector<double> CIm,ublas::matrix<double> evecs, ublas::vector<double> evals, ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);

void FT_Gt_heom_WZ(ublas::vector<double>& GReals, ublas::vector<double>& GImags, double ntime);
double DM_WZ_HEOM(int M, ublas::vector<double>& DMReals, ublas::vector<double> GReal, ublas::vector<double> GImag, double te, double ntime,ublas::vector<double> CIm,ublas::matrix<double> evecs, ublas::vector<double> evals, ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat);

#endif // LINESHAPE_HPP_INCLUDED
