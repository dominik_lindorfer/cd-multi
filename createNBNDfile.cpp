#include <string>
#include <iostream>
#include <fstream>
using namespace std;

void createNBNDfile(char *pdbname){

    //Open .pdb file and obtain all NB-ND coordinates
    ifstream pdbfile (pdbname);

    //allign a NB-ND file with all the corresponding coordinates
    ofstream nbndfile;
    nbndfile.open("nb_nd.dat",ios::out);



    //create loopstring & strings for stringsearch
    string str;
    string str2 ("NB  C");
    string str3 ("ND  C");
    string temp;

    if (pdbfile.is_open()){
        while (pdbfile.good()){
            getline (pdbfile,str);  //gets every line until EOF

        if (str.find(str2) != string::npos){
            //cout<<str<<endl;

            unsigned pos = str.find("1.00");    //maybe needs to be changed for other files
            unsigned pos1 = str.find("NB");

            temp=str.substr (pos1,pos-pos1);    //create substring
            //cout<<temp<<endl;
            nbndfile<<temp<<endl;
            }

        if (str.find(str3) != string::npos){
            //cout<<str<<endl;

            unsigned pos = str.find("1.00");     //maybe needs to be changed for other files
            unsigned pos1 = str.find("ND");

            temp=str.substr (pos1,pos-pos1);    //create substring
            //cout<<temp<<endl;
            nbndfile<<temp<<endl;
            }
        }
        pdbfile.close();
    }

    else cout << "Unable to open .pdb file";
}
