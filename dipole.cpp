#include "ublas.hpp"
#include "init.hpp"
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

void getdipoles(ublas::vector< ublas::vector< ublas::vector<double> > >& dipolemat, ublas::vector<double> debye_vec, ublas::vector<string>& chlname_vec){

    //At first let us obtain the unitvectors of the dipole transitionmoments from the NB-ND file
    //they will be stored as vectors within an ublas::vector

    ublas::vector< ublas::vector<double> > ei_dipole(0);    //Dipolevector of different ei-unitvectors
    ublas::vector< ublas::vector<double> > temp_ei_dipole;
    ublas::vector< ublas::vector<double> > RCenter_dipole(0);  //Center of Dipole

    ifstream nbndfile ("nb_nd.dat");    //open nb_nd.dat
    string str; //dummystring
    string tmpstr;
    //double ri;

    int k=0;        //helps to count
    ublas::vector<double> ri_dipole(3);
    ublas::vector<double> rj_dipole(3);

    if (nbndfile.is_open()){
        while (nbndfile.good()){
            getline (nbndfile,str);
            if(nbndfile){               //to avoid nbndfile.good() bug when using double getline!!
            stringstream sstest(str);   //convert string to stream to get out only first word

            sstest >> tmpstr;

            chlname_vec.resize(chlname_vec.size()+1);   //make space for a new element
            sstest >> chlname_vec(k);                   //get CHL || CLA || w/e into the namevector

            for(int i=2;i<4;i++){sstest >> tmpstr;} //get to 5th element
            for(int i=0;i<3;i++){sstest >> ri_dipole(i);}   //add elements to tmp-vector
            //for(int i=0;i<3;i++){cout<<ri_dipole(i)<<" ";}    //errorchecking
            //cout<<endl;

            getline (nbndfile,str);
            stringstream sstest1(str);      //define newstring cause stringstream sucks here

            for(int i=0;i<4;i++){sstest1 >> tmpstr;} //get to 5th element
            for(int i=0;i<3;i++){sstest1 >> rj_dipole(i);}   //add elements to tmp-vector
            //for(int i=0;i<3;i++){cout<<rj_dipole(i)<<" ";}    //errorchecking
            //cout<<endl;

            //cout<<ri_dipole-rj_dipole<<endl;
            ei_dipole.resize(ei_dipole.size()+1);
            ei_dipole(k)=(ri_dipole-rj_dipole)/norm_2(ri_dipole-rj_dipole);
            //cout<<(ri_dipole-rj_dipole)/norm_2(ri_dipole-rj_dipole)<<endl;
            RCenter_dipole.resize(RCenter_dipole.size()+1);
            RCenter_dipole(k)=1.0/2.0* (ri_dipole+rj_dipole);

            k++;
            }
            else break;
        }
    }
    //cout<<ei_dipole<<endl;          //errorchecking
    //cout<<RCenter_dipole<<endl;     //errorchecking

    //Dipolematrix has UnitTransition Dipolevectors in & The Centers of the CHL for RM
    dipolemat.resize(dipolemat.size()+2);

    dipolemat(0)=ei_dipole;
    dipolemat(1)=RCenter_dipole;

    //sets the dipolemoments according to init.dat debye_a, debye_b

    //cout<<"Dipolemat size: "<<dipolemat(0).size()<<endl;      //errorchecking
    //cout<<"Debye_Vec size: "<<debye_vec.size()<<endl;
    //cout<<"Chl_Namevec size: "<<chlname_vec.size()<<endl;
    //cout<<chlname_vec<<endl;
    //cout<<dipolemat(0)<<endl;

    for(int i = 0; i < dipolemat(0).size(); i++){
        if(chlname_vec(i) == "CLA"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(0);
        }
        if(chlname_vec(i) == "CHL"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(1);
        }
        if(chlname_vec(i) == "CLABy"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(2);
        }
        if(chlname_vec(i) == "CHLBy"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(3);
        }
        if(chlname_vec(i) == "CLAQx"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(4);
        }
        if(chlname_vec(i) == "CHLQx"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(5);
        }
        if(chlname_vec(i) == "CLABx"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(6);
        }
        if(chlname_vec(i) == "CHLBx"){
            dipolemat(0)(i) = dipolemat(0)(i) * debye_vec(7);
        }
    }

    //cout<<dipolemat(0)<<endl;
    //cout<<"dipolemat:"<<dipolemat<<endl;   //errorchecking
    //cout<<"dipolemat:"<<dipolemat(0)<<endl;   //errorchecking
    //cout<<chlname_vec<<endl;
}
