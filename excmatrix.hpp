#ifndef EXCMATRIX_HPP_INCLUDED
#define EXCMATRIX_HPP_INCLUDED
#include "ublas.hpp"
#include <string>
using namespace std;

void getSiteEnergies(ublas::vector<double>& sitevec, ublas::vector<string> chlname_vec);
void diagonalizeexcmatrix(ublas::matrix<double>& evec, ublas::vector<double>& eval, ublas::vector<string>& chlname_vec, ublas::vector<double>& parameter_vec, ublas::vector<double> sitevec);


#endif // EXCMATRIX_HPP_INCLUDED
