#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "four1.hpp"
#include "lineshape.hpp"
#include "ublas.hpp"

using namespace std;

fftw_plan p1_FFTW;
fftw_plan p2_FFTW;

void correlation(ublas::vector<double> parameter_vec, ublas::vector<double>& CIm){

    double te = 8192.0/512.0;   //8192.0 /256.0; //works
    double ntime = 8192.0*32;    //8192.0 * 32;   //works
    double delta = te / (ntime -1);
    double om,spe,spe1,spectral;
    double twopi = 3.141592 * 2.0;

    double spec[2 * (int)ntime + 1];

    for(int i = 1; i <= ntime / 2.0 -1; i++){
         om = twopi * i / delta / ntime;
         //spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
         //spe1 = spe;
         spe = om*om * (1.0 + n_WZ(om,parameter_vec(0))) * J_WZ(om);
         spe1 = om*om * n_WZ(om,parameter_vec(0)) * J_WZ(om);

         spec[2*i + 1] = spe;
         spec[2*(int)ntime - 2*i+1] = spe1;
         spec[2*i+2] = 0.;
         spec[2*(int)ntime - 2*i+2] = 0.;
    }

    spec[1] = 0.0;  //1.0 / (0.5*twopi);
    spec[2] = 0.0;
    spec[(int)ntime +1] = 0;
    spec[(int)ntime +2] = 0;

    int is = -1;
    double corr_real, corr_imag;

    four1_FFTW(spec, ntime, is);

    //ofstream myfile;
    //myfile.open ("corr.dat");


    double *corr = 0;
    corr = new double [2 * (int)ntime +1];

    for(int i = 1; i <= ntime; i++){

        corr_real = spec[2*i-1] / ntime * twopi / delta;
        corr_imag = spec[2*i] / ntime * twopi / delta;
        //myfile <<setprecision(15)<< (i-1)*delta/(twopi*3.0*pow(10,10)) <<" "<<corr_real<<" "<<corr_imag<<" "<<endl;
        corr[2*i-1] = corr_real;
        corr[2*i] = corr_imag;
    }
    //myfile.close();

    double re, im;
    for(int i = 1; i<=ntime/2.0 ; i++){
            re = corr[2*i-1];
            im = corr[2*i];
            spec[2*i-1] = re;
            spec[2*i] = im;
    }

    for(int i = ntime/2.0 +1; i<=ntime; i++){
            spec[2*i-1] = 0.0;
            spec[2*i] = 0.0;
    }

    is = 1;
    four1_FFTW(spec, ntime, is);

    //ofstream myfile1;
    //myfile1.open ("corrrueck.dat");

    //Prepare for passing to &vectors
    int k = 0;  //will go from 0 to ntime-1
    //CRe.resize((int) (ntime));
    CIm.resize((int) (ntime));

    //w-Frame from [-ntime/2.0 -1 , ntime/2.0 -1]
    //for ntime = 8192*32 -> [-51471.5 , 51471.5]  which is maybe a bit too rough for current problems

    for(int i = ntime/2.0 -1; i>=1;i = i-1){
            //myfile1<<-twopi*i/te<<" "<<spec[2*(int)ntime-2*i+1]*delta<<" "<<spec[2*(int)ntime-2*i+2]*delta<<" "<<endl;
            //CRe(k) = spec[2*(int)ntime-2*i+1]*delta;
            CIm(k) = spec[2*(int)ntime-2*i+2]*delta;
            k++;
    }

    for(int i = 0; i<=ntime/2-1;i++){
            //myfile1<<twopi*i/te<<" "<<spec[2 * i +1] * delta<<" "<<spec[2 * i +2] *delta<<" "<<endl;
            //CRe(k) = spec[2 * i +1]*delta;
            CIm(k) = spec[2 * i +2]*delta;
            k++;
    }
    //myfile1.close();
    delete[] corr;
    corr = 0;
}
