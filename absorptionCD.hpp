#ifndef ABSORPTIONCD_HPP_INCLUDED
#define ABSORPTIONCD_HPP_INCLUDED
#include "ublas.hpp"
void CDabsorption(ublas::matrix<double>& evecs, ublas::vector<double>& evals, ublas::vector< ublas::vector< ublas::vector<double> > >& dipolemat, ublas::vector<double> parameter_vec, ublas::vector<double>& CD, ublas::vector<double>& alpha, ublas::vector<double> CIm, ublas::vector<double> GReals, ublas::vector<double> GImags);
void crossprod(ublas::vector<double>& A,ublas::vector<double>& B,ublas::vector<double>& R);

#endif // ABSORPTIONCD_HPP_INCLUDED
