#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "ublas.hpp"
#include "createNBNDfile.hpp"
using namespace std;

void init(ublas::vector<double>& debye_vec,ublas::vector<double>& parameter_vec){

    //Init.dat will be analyzed
    ifstream init ("init.dat");
    string str;
    string pdbfilename;
    string tmp;
    double temp;

    //First Line of init.dat is pdbfile name!
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word
            sstest >> pdbfilename;
        }
    }

    //Second Line checks if NB-ND file needs to be set up
    if (init.is_open()){
        if (init.good()){
            getline (init,str);

            if (str.compare(0,1,"1") == 0){        //check if filecreation is enabled (1) or not (0)
                cout<<"NB-ND File will be created!"<<endl;
                cout<<".pdb filename is: "<<pdbfilename<<endl;

                char buffer[200];
                size_t length = pdbfilename.copy(buffer,str.length(),0);
                buffer[length]='\0';
                createNBNDfile(buffer);
            }
        }
    }

    //Third Line checks Dipolestrength of CHL & CLA     //this might need some changes to fit the dipolematrix
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word

            int k=0;                    //get every
            while(sstest >> temp){

                debye_vec.resize(debye_vec.size()+1);
                debye_vec(k)=temp;
                k++;
            }
            //cout<<debye_vec<<endl;    //errorchecking
        }
    }
    //Fourth Line gets set Temperature into the Parameter-Vector, parameter_vec
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word
            parameter_vec.resize(parameter_vec.size()+1);
            sstest >> parameter_vec(0);
            //cout<<debye_vec<<endl;    //errorchecking
        }
    }
    //Fifth Line gets the Vibration-Correlation Radius Rc into the Parameter-Vector, parameter_vec
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word
            parameter_vec.resize(parameter_vec.size()+1);
            sstest >> parameter_vec(1);
            //cout<<parameter_vec<<endl;    //errorchecking
        }
    }
    //Sixth Line get the FWHM for the static disorder
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word
            parameter_vec.resize(parameter_vec.size()+1);
            sstest >> parameter_vec(2);
            //cout<<parameter_vec<<endl;    //errorchecking
        }
    }

    //Seventh Line gets the number of static disorder loops
    if (init.is_open()){
        if (init.good()){
            getline (init,str);
            stringstream sstest(str);   //convert string to stream to get out only first word
            parameter_vec.resize(parameter_vec.size()+1);
            sstest >> parameter_vec(3);
            //cout<<parameter_vec<<endl;    //errorchecking
        }
    }
}
