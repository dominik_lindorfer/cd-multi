#include <cmath>
#include "ublas.hpp"
#include "correlation.hpp"
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <fftw3.h>
#include <complex>
#include <vector>
#include <cstdlib>

using namespace std;

/*double J_WZ(double w){  //Empirical Function of Spectral Density J(w) (2.16) in Wellenzahlen
    if( w >= 0){
        return ( 0.8/(5040.0 * 2 * pow(0.069e-3 / 6.582e-16 / 1.88496e11,4)) * pow(w,3) * exp(- sqrt(w / (0.069e-3 / 6.582e-16 / 1.88496e11))) + 0.5/(5040.0 * 2 * pow(0.24e-3 / 6.582e-16 / 1.88496e11,4)) * pow(w,3) * exp(- sqrt(w / (0.24e-3 / 6.582e-16 / 1.88496e11))) );
        //return ( s1/(5040.0 * 2 * pow(w1,4)) * pow(w,3) * exp(- sqrt(w / w1)) + s2/(5040.0 * 2 * pow(w2,4)) * pow(w,3) * exp(- sqrt(w / w2)) );
    }

    if( w < 0){
        return 0;
    }
}*/

double J_WZ(double w){      //Drude-Lorentz Spectraldensity

    double om = 0.0;
    double lambda = 0.005;
    double nu = 1.0 / (15.0 * pow(10.0,-12.0) ) / (3 * pow(10.0,10.0));

    double J;

    if(w >= 0){
        J = nu * lambda * w / (nu * nu + (w + om)*(w + om)) + nu * lambda * w / (nu * nu + (w - om)*(w - om));
        return J;
    }
    if(w < 0){
        return 0;
    }
}


double n_WZ(double w, double T){    //Bose-Einstein Distribution (2.10) in Wellenzahlen
    if (w > 1e-6){
        return ( 1.0 / ( exp( (6.582 * pow(10.0,-16) * 1.88496 * pow(10.0,11) * w) / (8.617 * pow(10,-5) * T ) ) - 1.0 ));
    }
    else{
        return 1.0;
    }

}


double C_Re_WZ (double w, double T){   //C_Re (4.15) in w [cm^-1]
        return( 3.1415926535897932384626434 * w*w * ((1.0 + n_WZ(w,T)) * J_WZ(w) + n_WZ(-w,T) * J_WZ(-w)) );
}

double y_MNKL (int M, int N, int K, int L, ublas::matrix<double> evecs,  ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){
    //yMNKL from Eq. (4.9)
    //cm(M) etc. are the eigencoefficients, Rmn is the distance between the chlorophylls in question
    double sum = 0;
    double Rmn;
    ublas::vector<double> distance;

    for(int i = 0; i < evecs.size1(); i++){
        for(int j = 0; j < evecs.size2(); j++){
            distance = (dipolemat(1)(i) - dipolemat(1)(j));
            Rmn = 1e-10 * norm_2 (distance);
            sum += exp(- Rmn / parameter_vec(1)) * evecs(M,i) * evecs(N,i) * evecs (K,j) * evecs(L,j);
            //cout<<"i = "<<i<<" j = "<<j<<" "<< /*exp(- Rmn / parameter_vec(1)) **/ evecs(M,i) * evecs(N,i) * evecs (K,j) * evecs(L,j)<<endl;
            //cout<<setprecision(15)<<evecs(M,i) * evecs(N,i) * evecs (K,j) * evecs(L,j)<<",";
        }
    }
    //errorchecking
    /*for(int i = 0; i < evecs.size1(); i++){
        cout<<evecs(L,i)<<",";
    }*/
    //cout<<evecs(0,0) * evecs(1,0) * evecs (2,0) * evecs(3,0)<<endl;
    //cout<<"Chl1: "<<dipolemat(1)(1)<<endl;
    //cout<<"Chl1: "<<dipolemat(1)(2)<<endl;
    //cout<<"InnerProd: "<<inner_prod(dipolemat(1)(1),dipolemat(1)(2))<<endl;
    //cout<<"Length: "<<dipolemat(1)(1)-dipolemat(1)(2)<<endl;
    //cout<<"Norm: "<<norm_2(dipolemat(1)(1)-dipolemat(1)(2))<<endl;
    return sum;
}

double tau_WZ (int M, ublas::vector<double> evals, ublas::matrix<double> evecs,  ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){
    double sum;
    for(int K = 0; K < evals.size(); K++){
        if( K != M){
            sum += 2.0 * y_MNKL(M,M,K,K,evecs,parameter_vec,dipolemat) * C_Re_WZ(evals(M) - evals(K) , parameter_vec(0) );  //parameter_vec(0) contains Temperature
        }   //C_Re_WZ_Modified muss ersetzt werden durch C_Re_WZ
    }
    sum = 1.0 / 2.0 * sum;
    return sum;
}

double wM0_WZ ( int M, ublas::vector<double> evals, ublas::matrix<double> evecs, ublas::vector<double> parameter_vec, ublas::vector<double> CIm, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){

    double sum;
    double wM0;

    //CorrelationFunction
    //ublas::vector<double> CIm, CRe;
    //correlation(parameter_vec, CRe, CIm);

    double te = 8192.0/512.0;
    double ntime = 8192.0*32;
    double twopi = 3.141592 * 2.0;
    int suche;
    double CImags;

    for( int K = 0; K < evals.size(); K++){
        if( K != M){

            suche = (evals(M) - evals(K)+ (twopi/te*(ntime/2.0 -1)))*te/(twopi);
            //cout<<"Suche wM0 = "<<suche<<endl;
            CImags = CIm(suche);
            sum += y_MNKL(M,M,K,K,evecs,parameter_vec,dipolemat) * CImags;   //* C_Im_WZ(evals(M) - evals(K),parameter_vec);
        }
    }

    //cout<<"M: "<<M<<" yMMMM: "<<y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat)<<" sum: "<<sum<<endl;

    wM0 = evals(M) - y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat) * 101.673 + sum;

    return( wM0 );
}

void FT_Gt_WZ(ublas::vector<double>& GReals, ublas::vector<double>& GImags, double te, double ntime, ublas::vector<double> parameter_vec){
    //see FT Test-Program for further INFO
    //double te = 8192.0 / 512.0;               //te is the time-interval used for delta; the smaller this is, the closer the interval for delta-w gets;
                                                //the interval you get is [-ntime/2 * delta , ntime/2 * delta]
                                                //if you want to increase the interval of the results, change this parameter
    //double ntime = 8192.0 * 16.0;             //this is N which defines the highest frequency we can obtain, it is w = (N-1) * delta * w; change for accurracy
    int N = ntime;    //gives the size of the Vectors you put into FFTW and get out of it; The bigger this is the more accurate the results
    double delta = te / (ntime -1);             //this sets the interval for frequency / time
    double twopi = 2.0 * 3.1415926535897932384626434;
    double spe,spe1,om;

    fftw_complex *in, *out;
    fftw_plan p;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);     //these vectors are empty, contrain 0's!
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    //Data alignment in in[i][j] accoriding to four1 from above
    for(int i = 1; i <= ntime/2 -1; i++){
         om = twopi * i / delta / ntime;                        //wir sind hier im k-Raum; deswegen muss spacing so gewählt werden
         spe = (1.0 + n_WZ(om,parameter_vec(0))) * J_WZ(om);
         spe1 = n_WZ(om,parameter_vec(0)) * J_WZ(om);

         //spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
         //spe1 = spe;

         in[i][0] = spe;
         in[(int)ntime - i][0] = spe1;

         in[i][1] = 0.;
         in[(int)ntime - i][1] = 0.;
    }

    p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);  //FFTW_MEASURE
    fftw_execute(p);
    //Output for Errorcorrections
    //ofstream myfile1;
    //myfile1.open ("corr1.dat");

    GReals.resize(ntime);
    GImags.resize(ntime);

    for(int i = 0; i < ntime; i++){
        GReals(i) = out[i][0] / ntime * twopi / delta;
        GImags(i) = out[i][1] / ntime * twopi / delta;
        //myfile1 <<setprecision(15)<< i*delta /(twopi * 3.0 * pow(10.0,10))/*/(pow(10,-15))*/ <<" "<<GReals(i)<<" "<<GImags(i)<<endl;    //<<" "<<exp(-i*delta)
    }
    //myfile1.close();
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}

void FT_Gt_heom_WZ(ublas::vector<double>& GReals, ublas::vector<double>& GImags,double ntime){
    /*Takes the G(t) Data Input directly from file: "gt_heom.data" which is generated by Mathematica*/
    /*Mathematica needs to take care of the ntime-Parameter                                         */
    GReals.resize(ntime);
    GImags.resize(ntime);

    for(int i = ntime/2.0; i < ntime; i++){
        GReals(i) = 0;
        GImags(i) = 0;
    }


    double delta;
    ifstream gt_heom;
    gt_heom.open("gt_heom.data");

    if (gt_heom.is_open()){
        if(gt_heom){
        for(int i = 0; i < ntime/2.0; i++){
            gt_heom >> delta;
            gt_heom >> GReals(i);
            gt_heom >> GImags(i);
            //cout<<delta<<" "<<GReals(i)<<" "<<GImags(i)<<endl;
        }
        }
    }
    gt_heom.close();
}


double DM_WZ(int M, ublas::vector<double>& DMReals,ublas::vector<double> GReal, ublas::vector<double> GImag, double te, double ntime, ublas::vector<double> CIm, ublas::matrix<double> evecs, ublas::vector<double> evals, ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){
    //Initialize Parameter
    ublas::vector<double> GReals;
    ublas::vector<double> GImags;
    //cout<<"FT_Gt_WZ"<<endl;
    //FT_Gt_WZ(GReals,GImags,te,ntime,parameter_vec);

    GReals = GReal * y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat);
    GImags = GImag * y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat);

    double GM0 = GReals(0);
    double TauM = tau_WZ(M,evals,evecs,parameter_vec,dipolemat);
    double wM0 = wM0_WZ(M,evals,evecs,parameter_vec,CIm,dipolemat);

    cout<<endl<<"TauM = "<<TauM;
    cout<<endl<<"WM0 = "<<wM0<<endl;

    int N = ntime;    //gives the size of the Vectors you put into FFTW and get out of it; The bigger this is the more accurate the results
    double delta = te / (ntime -1);             //this sets the interval for frequency / time
    double twopi = 2.0 * 3.1415926535897932384626434;
    double spe,spe1,om;
    double time;
    double re,im;

    fftw_complex *in, *out;
    fftw_plan p;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);     //these vectors are empty, contrain 0's!
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    for(int i = 0; i < ntime/2 ; i++){
         time = (i+1) * delta;
         re = GReals(i);
         im = GImags(i);

         //Shifted by wM0!
         spe = exp(re - time * TauM - GM0) * cos(im);
         spe1 = exp(re - time * TauM - GM0) * sin(im);

         //spe = exp(-GM0 + GReals(i) - time/TauM) * cos(GImags(i) - time * wM0);         //exp(re - GM0 - time * TauM) * cos(-wM0 * time + im);
         //spe1 = exp(-GM0 + GReals(i) - time/TauM) * sin(GImags(i) - time * wM0);        //exp(re - GM0 - time * TauM) * sin(-wM0 * time + im);

         /*om = twopi * i / delta / ntime;
         spe = (1.0 + n_WZ(om,parameter_vec(0))) * J_WZ(om);
         spe1 = n_WZ(om,parameter_vec(0)) * J_WZ(om);*/

         //spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
         //spe1 = spe;

         in[i][0] = spe;
         in[i][1] = spe1;

         //in[(int)ntime - i][0] = spe1;
         //in[(int)ntime - i][1] = 0.;
    }


    for(int i = ntime-1; i >= ntime/2; i = i-1){
        time = (i+1-ntime) * delta;
        re = GReals(i);
        im = GImags(i);

        //Shifted by wM0!
        spe = exp(re + time * TauM - GM0) * cos(im);
        spe1 = exp(re + time * TauM - GM0) * sin(im);

        //unshifed Plots
        //spe = exp(-GM0 + GReals(i) + time/TauM) * cos(GImags(i) + time * wM0);  //exp(re - GM0 + time * TauM) * cos(wM0 * time + im);
        //spe1 = exp(-GM0 + GReals(i) + time/TauM) * sin(GImags(i) + time * wM0); //exp(re - GM0 + time * TauM) * sin(wM0 * time + im);

        in[i][0] = spe;
        in[i][1] = spe1;
    }

    p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);  //FFTW_MEASURE
    fftw_execute(p);

    DMReals.resize(ntime);
    //cout<<"DMReals Size: "<<DMReals.size()<<endl;
    //DMImags.resize(ntime);

    double spectral;

    //Hier fehlt noch der Übertrag in Line(i) ...
    //Dm schaut für M = 1 aus wie eine Gaußkurve

    spectral = in[0][0] * delta;
    //DMReals[(int)ntime/2][0] = spectral / twopi;

    for(int i = 0; i < ntime/2.0; i++){
        spectral = out[i][0] * delta;
        DMReals(ntime/2.0 + i) = spectral / twopi;
        //myfile1 <<setprecision(15)<< i*delta <<" "<<GReals(i)<<" "<<GImags(i)<<endl;    //<<" "<<exp(-i*delta)
    }

    for(int i = ntime/2.0; i< ntime; i++){
        spectral = out[i][0] * delta;
        DMReals(i - ntime/2.0) = spectral / twopi;
    }

    //ofstream myfile;
    //myfile.open("Dm.dat");
    for(int i = 0; i<ntime; i++){
        //DMReals(i) = out[i][0] * delta / twopi;
        //myfile<<(i-ntime/2.0)*twopi/te<<" "<<DMReals(i)<<endl;
    }

    //Cut-out proper w-Range (this is a dirty solution!)
    //cout<<"Size of DMReals: "<<DMReals.size()<<endl;  //DMReals.size() is 4194304
    int upperlim = ntime/2.0  + te/twopi * (1.0/(300*1e-7) - wM0);      //approximatively this  works [600,730]!!!
    int lowerlim = ntime/2.0  + te/twopi * (1.0/(800*1e-7) - wM0);      //Check here for errors, (int) lower/upperlim might not be equal for all States M!
    /*
    double dupper = ntime/2.0  + te/twopi * (1.0/(300*1e-7) - wM0);
    double dlower = ntime/2.0  + te/twopi * (1.0/(800*1e-7) - wM0);

    cout<<"Ntime = "<<ntime<<endl;
    cout<<"te = "<<te<<endl;
    cout<<"2Pi = "<<twopi<<endl;
    cout<<"Shift: "<<(1.0/(300*1e-7) - wM0)<<endl;
    cout<<"Shift: "<<(1.0/(800*1e-7) - wM0)<<endl;

    */
    //cout<<"Difference Upper/Lower: "<<upperlim-lowerlim<<endl;          //Check here for errors, (int) lower/upperlim might not be equal for all States M!
    //241857
    //exit(1);
    //cout<<"DMReals Lower , Upper: "<<DMReals(lowerlim)<<" "<<DMReals(upperlim)<<endl;

    //Cut-out proper w-Range (this is a dirty solution!)
    ublas::vector<double> Temp(upperlim - lowerlim);
    //cout<<"Size of Temp: "<<Temp.size()<<endl;

    for(int i = 0; i < (upperlim-lowerlim); i++){

                Temp((int)Temp.size()-1 - i) = DMReals(lowerlim + i);
                //myfile<<((i+lowerlim)-ntime/2.0)*twopi/te<<" "<<Temp(i)<<endl;
                //Temp(Temp.size()-1 - i) = DMReals[lowerlim+i];    //works for [600,730]
                //myfile<<((i+lowerlim)-ntime/2.0)*twopi/te<<" "<<Temp(i)<<endl;
    }
    //myfile.close();
    //1697652
    DMReals.resize(1697652);    //Dirty Solution to get a hold of Error above, cutting out 1-2 values

    for(int i = 0; i < 1697652; i++){
            DMReals(i) = Temp(i);
    }


    //myfile1.close();
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}

double DM_WZ_HEOM(int M, ublas::vector<double>& DMReals,ublas::vector<double> GReal, ublas::vector<double> GImag, double te, double ntime, ublas::vector<double> CIm, ublas::matrix<double> evecs, ublas::vector<double> evals, ublas::vector<double> parameter_vec, ublas::vector< ublas::vector< ublas::vector<double> > > dipolemat){
    //Initialize Parameter
    ublas::vector<double> GReals;
    ublas::vector<double> GImags;
    //cout<<"FT_Gt_WZ"<<endl;
    //FT_Gt_WZ(GReals,GImags,te,ntime,parameter_vec);

    //Multiplied by -1 because of Definition in Mathematica-File!
    GReals = GReal * y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat) * (-1);
    GImags = GImag * y_MNKL(M,M,M,M,evecs,parameter_vec,dipolemat) * (-1);

    double GM0 = GReals(0);
    double TauM = tau_WZ(M,evals,evecs,parameter_vec,dipolemat);
    double wM0 = wM0_WZ(M,evals,evecs,parameter_vec,CIm,dipolemat);

    cout<<endl<<"TauM = "<<TauM;
    cout<<endl<<"WM0 = "<<wM0<<endl;

    int N = ntime;    //gives the size of the Vectors you put into FFTW and get out of it; The bigger this is the more accurate the results
    double delta = te / (ntime -1);             //this sets the interval for frequency / time
    double twopi = 2.0 * 3.1415926535897932384626434;
    double spe,spe1,om;
    double time;
    double re,im;

    fftw_complex *in, *out;
    fftw_plan p;

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);     //these vectors are empty, contrain 0's!
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    for(int i = 0; i < ntime/2 ; i++){
         time = (i+1) * delta;
         re = GReals(i);
         im = GImags(i);

         //Shifted by wM0!
         spe = exp(re - time * TauM - GM0) * cos(im);
         spe1 = exp(re - time * TauM - GM0) * sin(im);

         //spe = exp(-GM0 + GReals(i) - time/TauM) * cos(GImags(i) - time * wM0);         //exp(re - GM0 - time * TauM) * cos(-wM0 * time + im);
         //spe1 = exp(-GM0 + GReals(i) - time/TauM) * sin(GImags(i) - time * wM0);        //exp(re - GM0 - time * TauM) * sin(-wM0 * time + im);

         /*om = twopi * i / delta / ntime;
         spe = (1.0 + n_WZ(om,parameter_vec(0))) * J_WZ(om);
         spe1 = n_WZ(om,parameter_vec(0)) * J_WZ(om);*/

         //spe = 1.0 / (1.0 + om*om) / (0.5 * twopi);
         //spe1 = spe;

         in[i][0] = spe;
         in[i][1] = spe1;

         //in[(int)ntime - i][0] = spe1;
         //in[(int)ntime - i][1] = 0.;
    }


    for(int i = ntime-1; i >= ntime/2; i = i-1){
        time = (i+1-ntime) * delta;
        re = GReals(i);
        im = GImags(i);

        //Shifted by wM0!
        spe = 0.0;  //exp(re + time * TauM - GM0) * cos(im);
        spe1 = 0.0; //exp(re + time * TauM - GM0) * sin(im);

        //unshifed Plots
        //spe = exp(-GM0 + GReals(i) + time/TauM) * cos(GImags(i) + time * wM0);  //exp(re - GM0 + time * TauM) * cos(wM0 * time + im);
        //spe1 = exp(-GM0 + GReals(i) + time/TauM) * sin(GImags(i) + time * wM0); //exp(re - GM0 + time * TauM) * sin(wM0 * time + im);

        in[i][0] = spe;
        in[i][1] = spe1;
    }

    p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);  //FFTW_MEASURE
    fftw_execute(p);

    DMReals.resize(ntime);
    //cout<<"DMReals Size: "<<DMReals.size()<<endl;
    //DMImags.resize(ntime);

    double spectral;

    //Hier fehlt noch der Übertrag in Line(i) ...
    //Dm schaut für M = 1 aus wie eine Gaußkurve

    spectral = in[0][0] * delta;
    //DMReals[(int)ntime/2][0] = spectral / twopi;

    for(int i = 0; i < ntime/2.0; i++){
        spectral = out[i][0] * delta;
        DMReals(ntime/2.0 + i) = spectral / twopi;
        //myfile1 <<setprecision(15)<< i*delta <<" "<<GReals(i)<<" "<<GImags(i)<<endl;    //<<" "<<exp(-i*delta)
    }

    for(int i = ntime/2.0; i< ntime; i++){
        spectral = out[i][0] * delta;
        DMReals(i - ntime/2.0) = spectral / twopi;
    }

    ofstream myfile;
    myfile.open("Dm.dat");
    for(int i = 0; i<ntime; i++){
        //DMReals(i) = out[i][0] * delta / twopi;
        myfile<<(i-ntime/2.0)*twopi/te<<" "<<DMReals(i)<<endl;
    }
    myfile.close();
    //Cut-out proper w-Range (this is a dirty solution!)
    //cout<<"Size of DMReals: "<<DMReals.size()<<endl;  //DMReals.size() is 4194304
    int upperlim = ntime/2.0  + te/twopi * (1.0/(300*1e-7) - wM0);      //approximatively this  works [600,730]!!!
    int lowerlim = ntime/2.0  + te/twopi * (1.0/(800*1e-7) - wM0);      //Check here for errors, (int) lower/upperlim might not be equal for all States M!
    /*
    double dupper = ntime/2.0  + te/twopi * (1.0/(300*1e-7) - wM0);
    double dlower = ntime/2.0  + te/twopi * (1.0/(800*1e-7) - wM0);

    cout<<"Ntime = "<<ntime<<endl;
    cout<<"te = "<<te<<endl;
    cout<<"2Pi = "<<twopi<<endl;
    cout<<"Shift: "<<(1.0/(300*1e-7) - wM0)<<endl;
    cout<<"Shift: "<<(1.0/(800*1e-7) - wM0)<<endl;

    */
    //cout<<"Difference Upper/Lower: "<<upperlim-lowerlim<<endl;          //Check here for errors, (int) lower/upperlim might not be equal for all States M!
    //241857
    //exit(1);
    //cout<<"DMReals Lower , Upper: "<<DMReals(lowerlim)<<" "<<DMReals(upperlim)<<endl;

    //Cut-out proper w-Range (this is a dirty solution!)
    ublas::vector<double> Temp(upperlim - lowerlim);
    //cout<<"Size of Temp: "<<Temp.size()<<endl;

    for(int i = 0; i < (upperlim-lowerlim); i++){

                Temp((int)Temp.size()-1 - i) = DMReals(lowerlim + i);
                //myfile<<((i+lowerlim)-ntime/2.0)*twopi/te<<" "<<Temp(i)<<endl;
                //Temp(Temp.size()-1 - i) = DMReals[lowerlim+i];    //works for [600,730]
                //myfile<<((i+lowerlim)-ntime/2.0)*twopi/te<<" "<<Temp(i)<<endl;
    }
    //myfile.close();
    //1697652
    DMReals.resize(1697652);    //Dirty Solution to get a hold of Error above, cutting out 1-2 values

    for(int i = 0; i < 1697652; i++){
            DMReals(i) = Temp(i);
    }


    //myfile1.close();
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
}
