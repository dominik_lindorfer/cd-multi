//
//  ublas.hpp
//  FEMSchroed
//
//  Created by Stefan Janecek on 29.01.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef FEMSchroed_ublas_hpp
#define FEMSchroed_ublas_hpp

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/banded.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

namespace ublas = boost::numeric::ublas;


#endif
